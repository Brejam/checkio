__author__ = 'Ryan'
'''
Input: An original dictionary as a dict.
Output: The flattened dictionary as a dict.

Example:

flatten({"key": "value"}) == {"key": "value"}
flatten({"key": {"deeper": {"more": {"enough": "value"}}}}) == {"key/deeper/more/enough": "value"}
flatten({"empty": {}}) == {"empty": ""}
'''

def flatten(dictionary):
    stack = [((),dictionary)]
    result = {}
    while stack:
        path, current = stack.pop()

        for k, v in current.items():
            if v == {}:
                v = ""

            if isinstance(v, dict):
                stack.append((path + (k,), v))

            else:
                result["/".join((path + (k,)))] = v

    return result


dict1 = {"key": "value"}
dict2 = {"key": {"deeper": {"more": {"enough": "value"}}}}
dict3 = {"empty": {}}
dict4 = {"name": {
                    "first": "One",
                    "last": "Drone"},
                "job": "scout",
                "recent": {},
                "additional": {
                    "place": {
                        "zone": "1",
                        "cell": "2"}}}
print(flatten(dict3))