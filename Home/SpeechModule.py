__author__ = 'Ryan'
'''
Input: A number as an integer.

Output: The string representation of the number as a string.

Example:

checkio(4)=='four'

checkio(143)=='one hundred forty three'

checkio(12)=='twelve'

checkio(101)=='one hundred one'

checkio(212)=='two hundred twelve'

checkio(40)=='forty'
'''
FIRST_TEN = ["one", "two", "three", "four", "five", "six", "seven",
             "eight", "nine"]
SECOND_TEN = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
              "sixteen", "seventeen", "eighteen", "nineteen"]
OTHER_TENS = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy",
              "eighty", "ninety"]
HUNDRED = "hundred"

def checkio(number):

    n = number // 100
    t = [FIRST_TEN[n-1], HUNDRED] if n > 0 else []

    n = (number // 10) % 10
    t += [OTHER_TENS[n-2]] if n > 1 else []

    n = number % (10 if n > 1 else 20)
    t += [(FIRST_TEN+SECOND_TEN)[n-1]] if n > 0 else []

    return ' '.join(t)

# good solution. copied to understand
def ryanCopy(number):

    hundreds = number // 100
    words = [FIRST_TEN[hundreds-1], HUNDRED] if hundreds > 0 else []

    tens = (number // 10) % 10
    words += [OTHER_TENS[tens-2]] if tens > 1 else []

    units = number % (10 if tens > 1 else 20)
    words += [(FIRST_TEN+SECOND_TEN)[units-1]] if units > 0 else []
    print(hundreds, tens, units, words)

    return ' '.join(words)

print(ryanCopy(921))