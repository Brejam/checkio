__author__ = 'Ryan'
'''
Input: A positive integer.
Output: The product of the digits as an integer.

Example:

checkio(123405) == 120
checkio(999) == 729
checkio(1000) == 1
checkio(1111) == 1
'''


def checkio(num):
    total = 1
    for char in str(num).replace("0", ""):
        total *= int(char)
    return total

print(checkio(420))