__author__ = 'Ryan'
'''
Input: A number as a positive integer.
Output: The quantity of unities in the binary form as an integer.

Example:

checkio(4) == 1
checkio(15) == 4
checkio(1) == 1
checkio(1022) == 9
'''


def checkio(num):
    ones = 0
    for char in bin(num):
        if char == "1":
            ones += 1
    return ones


def checkio2(num):
    return bin(num).count("1")


checkio3 = lambda num: bin(num).count("1")


print(checkio3(1022))