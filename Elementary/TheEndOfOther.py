__author__ = 'Ryan'
'''
Input: Words as a set of strings.
Output: True or False, as a boolean.

Example:

checkio({"hello", "lo", "he"}) == True
checkio({"hello", "la", "hellow", "cow"}) == False
checkio({"walk", "duckwalk"}) == True
checkio({"one"}) == False
checkio({"helicopter", "li", "he"}) == False
'''
import itertools as it


def checkio(words_set):
    for combo in it.combinations(words_set, 2):
        if (combo[0].endswith(combo[1])) or (combo[1].endswith(combo[0])):
            return True
    return False


# clearer to read
def checkio2(words):
    for w1 in words:
        for w2 in words:
            if w1 != w2 and (w1.endswith(w2) or w2.endswith(w1)):
                return True
    return False

