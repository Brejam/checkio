__author__ = 'Ryan'
'''
Input: A sequence of strings as a tuple of strings (unicode).
Output: The text as a string.

Example:

left_join(("left", "right", "left", "stop")) == "left,left,left,stop"
left_join(("bright aright", "ok")) == "bleft aleft,ok"
left_join(("brightness wright",)) == "bleftness wleft"
left_join(("enough", "jokes")) == "enough,jokes"
'''


def left_join(phrases):
    """
        Join strings and replace "right" to "left"
    """
    return ",".join(phrases).replace("right", "left")

print(left_join(("brightness wright",)))