__author__ = 'Ryan'
'''
Input: Two arguments. A number as string and a radix as an integer.
Output: The converted number as an integer.

Example:

checkio("AF", 16) == 175
checkio("101", 2) == 5
checkio("101", 5) == 26
checkio("Z", 36) == 35
checkio("AB", 10) == -1
'''

digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
# print(len(digits))
# print(digits.index("A") * 16)
# 1     2     4     8     16
# 2**0  2**1  2**2  2**3  2**4


def checkio(num, radix):
    index = 1
    result = 0
    for char in num:
        if digits.index(char) >= radix:
            return -1
        result += radix ** (len(num) - index) * digits.index(char)
        index += 1
    return result


def checkio2(num, radix):
    try:
        return int(num, radix)
    except ValueError:
        return -1


def checkio3(*a):
    try: return int(*a)
    except ValueError: return -1
