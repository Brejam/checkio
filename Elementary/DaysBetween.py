from datetime import date

__author__ = 'Ryan'
'''
Input: Two dates as tuples of integers.
Output: The difference between the dates in days as an integer.

Example:

days_diff((1982, 4, 19), (1982, 4, 22)) == 3
days_diff((2014, 1, 1), (2014, 8, 27)) == 238
days_diff((2014, 8, 27), (2014, 1, 1)) == 238
'''

MONTHS = [31, 30, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


def days_diff(date1, date2):
    """
    Find absolute diff in days between dates
    """
    date1 = date(date1[0], date1[1], date1[2])
    date2 = date(date2[0], date2[1], date2[2])

    return abs(date1 - date2).days


# use * to unpack args from variable
def days_diff2(date1, date2):
    return abs(date(*date1) - date(*date2)).days



print(days_diff((1982, 4, 19), (1982, 4, 22)))
print(days_diff2((1982, 4, 19), (1982, 4, 22)))
print(days_diff((2014, 1, 1), (2014, 8, 27)))
print(days_diff2((2014, 1, 1), (2014, 8, 27)))
print(days_diff((2014, 8, 27), (2014, 1, 1)))
print(days_diff2((2014, 8, 27), (2014, 1, 1)))
