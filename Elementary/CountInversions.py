__author__ = 'Ryan'
'''
Input: A sequence as a tuple of integers.
Output: The inversion number as an integer.

Example:

count_inversion((1, 2, 5, 3, 4, 7, 6)) == 3
count_inversion((0, 1, 2, 3)) == 0
count_inversion((5, 3, 2, 1, 0)) == 10
'''
# nice use of itertools.combinations
import itertools as it


def count_inversion2(sequence):
    return sum(x > y for x, y in it.combinations(sequence, 2))


def count_inversion(numbers):
    inversions = 0
    for num in range(len(numbers)-1):
        for pair in range(num, len(numbers), 1):
            if numbers[num] > numbers[pair]:
                inversions += 1

    return inversions

print(count_inversion((5,4,3,2,1)))